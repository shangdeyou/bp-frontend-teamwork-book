# 2.6.Vue Router

### 通用架構

1. 主要路由
2. 非正式機才加入的路由
3. 例外處理路由
4. 換頁捲軸行為
5. middleware
6. 錯誤處理

### 主檔範例

```js
Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: (() => {
    // 1.主要路由
    let routes = mainRoutes
    // 2.非正式機才加入的路由
    if (process.env.VUE_APP_MODE !== 'build') {
      routes = routes.concat(testingRoutes)
    }
    // 3.例外處理路由
    routes = routes.concat(exceptionRoutes)
    return routes
  })(),
  scrollBehavior: function (to) {
    // 4.換頁捲軸行為（移畫面到最上面）
    if (to.hash) {
      return window.scrollTo({
        top: document.querySelector(to.hash).offsetTop,
        behavior: 'smooth'
      })
    } else {
      return window.scrollTo({
        top: 0,
        behavior: 'smooth'
      })
    }
  }
})

// 5.middleware
router.beforeEach(middleware)

// 6.錯誤處理
router.onError((error) => {
  const pattern = /router.onError/g
  const isChunkLoadFailed = error.message.match(pattern)
  const targetPath = router.history.pending.fullPath
  if (isChunkLoadFailed) {
    router.replace(targetPath)
  }
})

export default router
```
