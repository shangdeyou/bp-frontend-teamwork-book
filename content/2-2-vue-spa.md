# 架設Vue.js SPA專案

## 一、vue-cli網站建置步驟

### vue-cli建立專案

有兩種方式，用指令或vue ui

1. 執行gui介面 ：vue ui

2. 指令：`vue create <project-name>`

### vue-cli v3.10.0 安裝範例說明

1. 第一次操作時會是空的，如果曾經儲存過設定檔，可直接選擇設定檔建立專案

![](img/vue-spa_2.png)

1. 選擇安裝套件

![](img/vue-spa_3.png)

3. vue-router是否是使history模式

![](img/vue-spa_4.png)

4. 選擇CSS pre-processor

![](img/vue-spa_5.png)

5. 選擇Lint

![](img/vue-spa_6.png)

6. Lint檢查時機，建議選擇Lint on save即可

![](img/vue-spa_7.png)

7. 設定檔分開或者合併

![](img/vue-spa_8.png)


## 二、環境相關設定

### 開發模式及生產模式

- 開發模式在本機端建立本機server，可熱更新並能使用debug tool
    * `yarn serve`
    * 或  `npm serve`
- 生產模式透過webpack打包成可執行之靜態檔案，但需放上web server並設置正確路徑（如需在本機測試可用vscode的live server）
    * `yarn build`
    * 或  `npm build`

### 環境變數

- 必須以VUE_APP_開頭
- 在src/底下的js中使用環境變數範例：process.env.VUE_APP_SECRET

不同模式的環境變數
![](img/deployment_1.png)

資料來源：https://segmentfault.com/a/1190000015133974

### 增加staging模式

- Step1 增加stage模式(自訂)
```
"dev-build": "vuue-cli-service build --mode development"
```
- Step2 建立.env.stage檔
```
NODE_ENV = 'production’
VUE_APP_MODE = 'stage’
outputDir = 'stage’
```
- Step3 在vue.config.js設定輸出路徑
```
module.exports = {
  outputDir: process.env.outputDir
}
```

### 使用環境變數範例
```
let baseUrl: string = ''
switch (process.env.VUE_APP_MODE) {
  case 'serve':
    baseUrl = 'http://localhost:57156/api/'
    break
  case 'build':
    baseUrl = 'http://choose.blueplanet.com.tw/api/'
    break
}
export default baseUrl
```

### 改變網頁logo
```
<link rel="icon" href="<%= BASE_URL %>favicon2.ico">
```

- `<%= BASE_URL %>` 對應vue.config.js所設定根目錄路徑
- favicon.ico為預設路徑，都會在專案編譯時被vue預設logo給覆蓋過去，所以需更改他的名稱才能替換掉（有沒有人能幫忙研究其他更改方式？）

### Router的 History mode
- 無使用history mode之網址→  http://domain-name/index.html#/login
- history mode之網址→  http://domain-name/login
- History模式預設是關閉的，如要使用必須增加 `mode: 'history’`
```
const router = newVueRouter({
  mode: 'history',
  routes: [...]
})
```

- 使用history mode時，web server必須將vue網站路徑指向根目錄才能使用否則會失效

### 改變根目錄路徑方式
- Step1 在vue.config.js中增加路徑變數baseUrl（如不設置的話預設就是根目錄）
- Step2 public/index.html中所引入的檔案路徑前都要加上
    `<%= BASE_URL %>`
    例`<script src="<%= BASE_URL %>wasa/general.js"></script>`

### 如有修改根目路徑，又使用history mode的話需設定web server（apache/nginx）

設定方法請參考Vue官網：

https://router.vuejs.org/zh/guide/essentials/history-mode.html#%E5%90%8E%E7%AB%AF%E9%85%8D%E7%BD%AE%E4%BE%8B%E5%AD%90

## 三、檔案架構

### src資料夾結構
```
src
  ├──apis // API控制器
  ├──assets // 靜態資料（使用webpack打包）
  ├──components // 共用components
  ├──const // 共用常數
  ├──directives // vue directives
  ├──filters // vue filters
  ├──mixins // vue mixins
  ├──mock // mock.js
  ├──router // vue-router
  ├──storage // localStorage/sessionStorage/Cookie等 
  ├──store // vuex
  ├──utils // 共用js函式
  ├──vendor // 或plugins
  └──views // 路由對應的components
```

### 靜態檔案放置方式

1. 不使用webpack打包之靜態資源皆放在public/資料夾下，例：
    * .js (通常為直接執行的script)
    * .css (通常為全域樣式)
    * 圖片檔

2. 使用webpack打包之資源皆放在assets/資料夾下，例：
    * .js (通常為js module)
    * .css/.scss
    * .vue

### views與components資料夾的差別

* views資料夾放置router-view對應的component、或者較且區塊也不會重用的component。
* components資料夾放置可重用的component。

### views資料夾/檔案架構

基本上參考Nuxt的命名規則，Nuxt特有的規則除外，說明如下：

1. 資料夾名稱對應網址路徑"/name/"，資料夾可以是多層的。
2. 原則上vue檔案名稱對應最後一層的網址路徑，但如果檔名為Index.vue則最後一層網址路徑為該檔所在的資料夾名稱。

範例：

```
views
  ├──management
  |   ├──Admin.vue
  |   ├──Index.vue
  |   └──Personal.vue
  ├──Index.vue
  ├──Login.vue
  └──Search.vue
```

### components資料夾/檔案架構

每個component需建立一個相同名稱的資料夾來放置。另外.vue檔需符合命名規範。

範例：

```
components
  ├──AppTripleClickButton
  |   ├──AppTripleClickButton.vue
  ├──CompanyList
  |   ├──CompanyList.vue
  |   └──CompanyListCard.vue
  ├──ManagersTable
  |   └──ManagersTable.vue
  ├──TheLoginForm
  |   └──TheLoginForm.vue
```