# 1.2.Javascript(ES6以上)

## 一、基本命名規範

### 縮排規則（優先級C，2020/1/3）

縮排使用使用2個空格，不要使用tabs。

建議將編輯器設定預設tab取代為空格，並且縮排使用2個空格而非4個空格。

註：參考Google Javascript Style Guide

### 命名風格（優先級C，2020/1/3）

1. 變數及函式使用小駝峰(camelCase)

2. 物件屬性原則上使用小駝峰(camelCase)，如果有特定需求（比如說需要與api欄位命名一致）可使用_(下底線)連接

例如：

```js
let userInfo = {
  user_name: 'thunder',
  user_phone: '0912345678'
}  
```

3. 「共用」或「全域」的常數使用全部大寫，多個單字之間用_(下底線)連接

例如：`const DEFAULT_COUNTRY = 'taiwan'`

### 字串符號使用單引號而不是雙引號（優先級C，2020/1/3）

### 在js module中使用箭頭語法取代"function"（優先級C，2020/1/3）

箭頭函式與"function"當中的this指向對象不同，為避免混淆以箭頭函式為主。

## 二、程式規範

## 三、程式通用慣例

### 避免使用波動拳語法（優先級C，2020/1/3）

1. 盡量減少多層的if判斷

例：

```js
if (result != null) {
  if (result.payload != null) {
    if (result.payload.company != null) {
      // 略
    }
  }
}
```

可改為

```js
if (result && result.payload && result.payload.company) {
  // 略
}
```

2. 減少不必要的if/else區塊

例：

```js
if (success == true) {
  // 略
} else {
  alert('查詢失敗')
}
```

可改為

```js
if (success == false) {
  alert('查詢失敗')
  return
}
// 略
```

此範例改成後者的寫法除了可以省略掉else區塊之外，也能夠提升程式的可讀性，不需要將整個函式看完才能知道此例外狀態的任務。

3. 善用async/await

例：

```js
function ajax () {
  api.search()
    .then(data => {
      if (data.success == true) {
        // 略
      } else {
        alert('查詢失敗')
      }
    })
}
```

可改為
```js
async function ajax () {
  const result = await api.search()
  if (!result || result.success != true) {
    alert('查詢失敗')
    return
  }
  // 略
}
```