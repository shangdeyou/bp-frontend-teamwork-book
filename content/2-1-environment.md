# 2.1.開發環境

## 一、一般安裝

### 主要環境：
- node.js版本使用v8.9以上  https://nodejs.org/en/
- vue 2
- vue-cli 3.0 以上
- webpack 4


### 選擇安裝：

yarn (強列建議，速度有差) 

`npm install -g yarn  `

** yarn 可能有些bug，目前使用yarn的部分，只會在yarn serve和yarn build使用，其他安裝皆使用npm install

### 安裝Vue:

`npm install vue`（或`yarn global add vue`）


### 安裝webapck

`npm install webpack -g`

`npm install webpack-cli -g`

舊版只需安裝webpack即可，但最新的webpack 4需連同webpack-cli都要安裝才不會有問題

### 安裝vue-cli

`npm install -g @vue/cli`（或`yarn global add @vue/cli`）

確認版本：`vue -V`

![](img/vue-spa_1.png)

↑ 上圖：重要！不要裝到舊版的了

## 二、Javascript風格規範相關工具（選擇性使用）

### 風格規範

如果要在專案中加入風格規範工具，請使用Prettier，相關環境安裝說明如下：

VSCode安裝prettier擴充工具後，在程式內按下`alt + shift + f`可自動將程式排版。

如果要搭配eslint工具，使用vue-cli建立專案時lint工具選擇prettier即可。如果要自行安裝 `npm i -D eslint-plugin-prettier`。在.eslintrc.js設定檔中加入以下即可使用prettier規範：

```js
{
  "plugins": ["prettier"],
  "rules": {
    "prettier/prettier": "error"
  }
}
```

並且，未避免影響模板中的html格式，需在VSCode設定檔中加入以下：

```js
"vetur.format.defaultFormatterOptions": {
    "js-beautify-html": {
        "wrap_attributes": "force"
    }
  },
"vetur.format.defaultFormatter.html": "js-beautify-html",
```

### eslint相關設定（選擇性使用）

要記得先在vscode裝eslint

eslint在vscode中如無作用之解決方式：

![](img/environment_1.png)


### js檔預設縮排2個字元

編輯器下方會顯示目前縮排空格數，點下去可進行設定，但這個方法是每次開啟新檔都要各別設定的。

從設定選項設定後即為永久套用的預設值

![](img/environment_2.png)

![](img/environment_3.png)